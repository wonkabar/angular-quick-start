#!/bin/bash
set -e
# if [ -d /node/$APP_NAME ]; then
# 	echo $APP_NAME
# fi
# cd /node/apps/$APP_NAME

loc=${APP_NAME:-none}
if [ $loc == 'none' ]; then
	echo no app stated. Exiting.
	exit 1;
else
	echo "yes app stated: $loc"
	if [ ! -d "/node/apps/$loc" ]; then
		echo "creating app: $loc"
		ng new $loc
		cd $loc
		line=$(grep "\"serve\"" angular.json -n | cut -f1 -d:)
		line=$((line+3))
		sed -i "${line}s/$/,\n\"port\": 4200,\n\"host\": \"0.0.0.0\"/" angular.json

	else
		cd /node/apps/$loc
		echo "$loc project exists!"
	fi
fi
echo "starting $loc"
exec "$@"
