# angular-quick-start


## Using the included docker-compose.yml file

# THIS IS COPIED FROM AN OLD README. NEEDS TO BE UPDATED FOR THIS REPO
1. run command: `docker-compose up -d`


## Running more than one instance of this container on your machine

In order to run multiple instances of this container you need to specify unique ports for each instance.

To do this, simply change the host port mapping  in the `docker-compose.yml` file

#### Example

##### docker-compose.yml

```yaml
ports:
  - "4201:4200"
```
